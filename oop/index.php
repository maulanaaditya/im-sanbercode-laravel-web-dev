<?php 
    // require 'Animal.php';
    // require 'Ape.php';
    // require 'Frog.php';
    spl_autoload_register(function($class_name) {
        include $class_name . '.php';
    });

    $sheep = new Animal("shaun");
    
    // echo $sheep->name."<br>";
    // echo $sheep->legs."<br>";
    // echo $sheep->cold_blooded. "<br>";

    // Bisa Akses karena method get
    echo $sheep->get_name();
    echo "<br>";
    echo $sheep->get_legs();
    echo "<br>";
    echo $sheep->get_blooded();  
    echo "<br>";
    
    // Object Ape
    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); 

    echo "<br>";

    // Object Frog
    $kodok = new Frog("buduk");
    $kodok->jump() ;    

?>