<?php
    class Animal {
        protected $legs = 4;
        protected $cold_blooded = 'no';
        protected $name;

        public function __construct($paramName)
        {
            $this->name= $paramName;
        }

        // Getter Function
        public function get_legs() {
            // return value dari property
            return $this->legs;
        }

        public function get_name() {
            // Value sudah diisi dulu oleh construct
            return $this->name;
        }

        public function get_blooded() {
            // return value dari property
            return $this->cold_blooded;
        }
    }
?>