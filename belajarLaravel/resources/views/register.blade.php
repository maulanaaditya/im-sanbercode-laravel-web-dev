<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
</head>

<body>
    <form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign up Form</h3>
        <label>First name:</label>
        <br /><br />
        <input type="text" name="first_name" />
        <br /><br />
        <label>Last name:</label>
        <br /><br />
        <input type="text" name="last_name" />
        <br /><br />
        <label>Gender:</label>
        <br /><br />
        <input type="radio" name="gender" />
        <label>Male</label>
        <br />
        <input type="radio" name="gender" />
        <label>Female</label>
        <br />
        <input type="radio" name="gender" />
        <label>Other</label>
        <br /><br />
        <label>Nationality</label>
        <br /><br />
        <select name="nationality">
            <option value="1">Indonesia</option>
            <option value="2">Singapura</option>
            <option value="3">Tailand</option>
            <option value="3">Mesir</option>
        </select>
        <br /><br />
        <label>Language Spoken</label>
        <br />
        <input type="checkbox" name="language" />
        <label>Bahasa Indonesia</label>
        <br />
        <input type="checkbox" name="language" />
        <label>English</label>
        <br />
        <input type="checkbox" name="language" />
        <label>Other</label>
        <br /><br />
        <label>Bio</label>
        <br /><br />
        <textarea name="bio" cols="30" rows="10"></textarea>
        <br />
        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>
