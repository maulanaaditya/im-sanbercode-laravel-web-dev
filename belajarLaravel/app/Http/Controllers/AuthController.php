<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome(Request $request) {
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $gender = $request->input('gender');
        $nationality = $request->input('nationality');
        $language = $request->input('language');
        $bio = $request->input('bio');

        return view('welcome', compact('first_name', 'last_name', 'gender', 'nationality', 'language', 'bio'));
    }

  
}
