@extends('layout.masterlayout')

@section('title')
    Detail : {{ $cast->name }}
@endsection

@section('content')
    <table class="table">
        <tbody>
            <tr>
                <th scope="row">Nama</th>
                <td>{{ $cast->name }}</td>
            </tr>
            <tr>
                <th scope="row">Umur</th>
                <td>{{ $cast->umur }}</td>
            </tr>
            <tr>
                <th scope="row">Bio</th>
                <td>{{ $cast->bio }}</td>
            </tr>

        </tbody>
    </table>
@endsection
