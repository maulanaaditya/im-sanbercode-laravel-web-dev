@extends('layout.masterlayout')

@section('title')
    Edit Cast : {{ $cast->name }}
@endsection

@section('content')
    <form action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="name" value="{{ $cast->name }}" class="form-control" placeholder="Nama">
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="number" name="umur" value="{{ $cast->umur }}" class="form-control" placeholder="Umur">
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" class="form-control" id="" cols="30" rows="5">{{ $cast->bio }}</textarea>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
@endsection
