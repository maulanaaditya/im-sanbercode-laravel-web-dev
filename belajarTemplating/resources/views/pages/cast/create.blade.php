@extends('layout.masterlayout')

@section('title')
    Buat Cast Baru
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="name" class="form-control" placeholder="Nama">
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="number" name="umur" class="form-control" placeholder="Umur">
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" class="form-control" id="" cols="30" rows="5"></textarea>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection
