@extends('layout.masterlayout')

@section('title')
    Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm mb-3">Buat Cast Baru</a>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>

            @forelse ($casts as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>

                        <form action="/cast/{{ $item->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center">Tidak Ada Data Cast</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
